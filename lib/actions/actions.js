"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.Actions = Actions;

var _lodash = require("lodash");

var _regex = require("./../helpers/regex");

var _consts = require("./../config/consts");

function Actions() {
  if (!(this instanceof Actions)) {
    return new (Function.prototype.bind.apply(Actions, [null].concat(Array.prototype.slice.call(arguments))))();
  }

  var self = this;

  this.actions = {};

  this.add = function ($) {
    return function (actionsObj) {
      var actionsObjList = actionsObj;
      if ((0, _lodash.isObject)(actionsObj) && !(0, _lodash.isArray)(actionsObj)) actionsObjList = [actionsObj];
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = actionsObjList[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var _actionsObj = _step.value;

          if ((0, _lodash.isFunction)(_actionsObj)) _actionsObj = _actionsObj($.$trunks);
          if ((0, _lodash.isObject)(_actionsObj)) self.actions = Object.assign(_actionsObj, self.actions);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return self.actions;
    };
  };

  this.load = function ($) {
    return function (actionsObj) {
      var actions = self.add($)(actionsObj);
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        var _loop = function _loop() {
          var _step2$value = _slicedToArray(_step2.value, 2),
              actionName = _step2$value[0],
              actionObj = _step2$value[1];

          if (actionObj && actionObj.fn && (0, _lodash.isFunction)(actionObj.fn)) {
            $.prototype["" + _consts.FNACTIONS_PREFIX + actionName] = function () {
              var _this = this;

              this.toValue = this.toString = this.$value = this.$val = function () {
                return _this[_consts.NODE_EDITABLEVALUE];
              };
              if (!this[_consts.NODE_EDITABLEVALUE]) this[_consts.NODE_EDITABLEVALUE] = this.text() || "";
              try {
                var _actionObj$fn;

                for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                  args[_key] = arguments[_key];
                }

                this[_consts.NODE_EDITABLEVALUE] = (_actionObj$fn = actionObj.fn).call.apply(_actionObj$fn, [this, this[_consts.NODE_EDITABLEVALUE]].concat(args));
              } catch (error) {
                // console.error(`[Error on action fn: ${actionName}] ${error.message}`)
              } finally {
                return this;
              }
            };

            if (actionObj.alt && (0, _lodash.isArray)(actionObj.alt)) {
              var _iteratorNormalCompletion3 = true;
              var _didIteratorError3 = false;
              var _iteratorError3 = undefined;

              try {
                for (var _iterator3 = actionObj.alt[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                  var alternativeActionName = _step3.value;

                  $.prototype["" + _consts.FNACTIONS_PREFIX + alternativeActionName] = $.prototype["" + _consts.FNACTIONS_PREFIX + actionName];
                }
              } catch (err) {
                _didIteratorError3 = true;
                _iteratorError3 = err;
              } finally {
                try {
                  if (!_iteratorNormalCompletion3 && _iterator3.return) {
                    _iterator3.return();
                  }
                } finally {
                  if (_didIteratorError3) {
                    throw _iteratorError3;
                  }
                }
              }
            }
          }
        };

        for (var _iterator2 = Object.entries(actions)[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          _loop();
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }
    };
  };

  // this.play = function(data, actionValue, options = {}) {
  //   if (actionValue && actionValue !== "") {
  //     let [actionName, actionParams] = functionNameAndParameters(actionValue)
  //     if (actionName) {
  //       for (let actionKey in self.actions) {
  //         let keywords = [actionKey]
  //         let alternativeKeywords = self.actions[actionKey].alt || []
  //         keywords.push(...alternativeKeywords)

  //         if (keywords.some(keyword => actionValue.includes(keyword))) {
  //           let allOptions = Object.assign({ value: actionValue }, options)
  //           return self.actions[actionKey].fn(data, allOptions)
  //         }
  //       }
  //     }
  //   }
  //   return data
  // }
}

exports.default = new Actions();