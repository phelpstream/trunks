"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.default = function ($trunks) {
  var _$trunks$helpers$rege = $trunks.helpers.regex,
      functionParameters = _$trunks$helpers$rege.functionParameters,
      getRegexGroupValues = _$trunks$helpers$rege.getRegexGroupValues;
  var extractVarsFromJsCode = $trunks.helpers.string.extractVarsFromJsCode;
  var iterate = $trunks.helpers.iterate.iterate;
  var _$trunks$consts$regex = $trunks.consts.regexes,
      MULTI_PARENTHESIS = _$trunks$consts$regex.MULTI_PARENTHESIS,
      WIDER_PARENTHESIS = _$trunks$consts$regex.WIDER_PARENTHESIS,
      WIDER_JSON = _$trunks$consts$regex.WIDER_JSON,
      NUMBERS = _$trunks$consts$regex.NUMBERS,
      EMAIL = _$trunks$consts$regex.EMAIL,
      PHONE = _$trunks$consts$regex.PHONE,
      WEBSITE = _$trunks$consts$regex.WEBSITE;


  return {
    // condition: {
    //   alt: ['if'],
    //   fn: iterate((data, [condition], { multiple } = {}) => {
    //     let result = (function(data, condition){
    //       return eval(`${data}${condition}`)
    //     })()
    //   })
    // },

    split: {
      fn: iterate(function (data, _ref) {
        var _ref3 = _slicedToArray(_ref, 1),
            stringSplitter = _ref3[0];

        var _ref2 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref2.multiple;

        var splitData = void 0;
        if (stringSplitter && data && data.includes(stringSplitter)) {
          splitData = data.split(stringSplitter);
        } else if (data.includes(" ")) {
          splitData = data.split(" ");
        } else {
          throw new Error("no data to split");
        }
        return splitData.map(function (x) {
          return x.trim();
        }).filter(function (x) {
          return x !== "";
        });
      })
    },

    between: {
      fn: iterate(function (data, _ref4) {
        var _ref6 = _slicedToArray(_ref4, 2),
            betweenA = _ref6[0],
            andB = _ref6[1];

        var _ref5 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref5.multiple;

        if (betweenA && andB) {
          return data.split(betweenA).pop().split(andB).shift();
        }
        return data;
      })
    },

    after: {
      fn: iterate(function (data, _ref7) {
        var _ref9 = _slicedToArray(_ref7, 1),
            afterString = _ref9[0];

        var _ref8 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref8.multiple;

        return afterString ? data.split(afterString).pop() : data;
      })
    },

    before: {
      fn: iterate(function (data, _ref10) {
        var _ref12 = _slicedToArray(_ref10, 1),
            beforeString = _ref12[0];

        var _ref11 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref11.multiple,
            value = _ref11.value;

        return afterString ? data.split(afterString).shift() : data;
      })
    },

    trim: {
      fn: iterate(function (data, _) {
        var _ref13 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref13.multiple;

        return isString(data) ? data.trim() : null;
      })
    },

    join: {
      fn: iterate(function (data, _ref14) {
        var _ref16 = _slicedToArray(_ref14, 1),
            joinerString = _ref16[0];

        var _ref15 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref15.multiple;

        if ((0, _lodash.isArray)(data)) {
          if (joinerString) return data.join(joinerString);
          return data.join(" ");
        }
        return data;
      }, {
        onTypes: ["array"]
      })
    },

    lowercase: {
      alt: ["lcase"],
      fn: iterate(function (data, _) {
        var _ref17 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            _ref17$multiple = _ref17.multiple,
            multiple = _ref17$multiple === undefined ? false : _ref17$multiple;

        return data.toLowerCase();
      })
    },

    uppercase: {
      alt: ["ucase"],
      fn: iterate(function (data, _) {
        var _ref18 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            _ref18$multiple = _ref18.multiple,
            multiple = _ref18$multiple === undefined ? false : _ref18$multiple;

        return data.toUpperCase();
      })
    },

    number: {
      alt: ["nb"],
      fn: iterate(function (data, _) {
        var _ref19 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref19.multiple;

        if (multiple) return data.match(NUMBERS) || "";
        return data.match(NUMBERS) !== null ? data.match(NUMBERS)[0] : "";
      })
    },

    numbers: {
      alt: ["nbs"],
      fn: iterate(function (data, _) {
        var _ref20 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref20.multiple;

        var numbers = data.match(NUMBERS);
        if (numbers !== null) return numbers.join(" ");
        return null;
      })
    },

    words: {
      alt: ["w"],
      fn: iterate(function (data, _) {
        var _ref21 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref21.multiple;

        return data.replace(NUMBERS, " ");
      })
    },

    noescapchar: {
      alt: ["nec"],
      fn: iterate(function (data, _) {
        var _ref22 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref22.multiple;

        return data.replace(/\t+|\n+|\r+/gm, " ");
      })
    },

    right: {
      fn: iterate(function (data, _ref23) {
        var _ref25 = _slicedToArray(_ref23, 1),
            index = _ref25[0];

        var _ref24 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref24.multiple;

        if (Array.isArray(data)) return data.slice(data.length - index, data.length);
        return data.substr(data.length - index);
      }, { onTypes: ["array", "string"] })
    },

    left: {
      fn: iterate(function (data, _ref26) {
        var _ref28 = _slicedToArray(_ref26, 1),
            index = _ref28[0];

        var _ref27 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref27.multiple;

        if (Array.isArray(data)) return data.slice(0, index);
        return data.substr(0, index);
      }, { onTypes: ["array", "string"] })
    },

    fromto: {
      fn: iterate(function (data, _ref29) {
        var _ref31 = _slicedToArray(_ref29, 2),
            fromIndex = _ref31[0],
            toIndex = _ref31[1];

        var _ref30 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref30.multiple;

        if (Array.isArray(data)) return data.slice(fromIndex, toIndex + 1);
        return data.substr(fromIndex, toIndex);
      }, { onTypes: ["array", "string"] })
    },

    get: {
      fn: iterate(function (data, _ref32) {
        var _ref34 = _slicedToArray(_ref32, 1),
            index = _ref34[0];

        var _ref33 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref33.multiple;

        if (Array.isArray(data) && data[nb - 1]) return data[nb - 1];
        return data.charAt(nb);
      }, { onTypes: ["array", "string"] })
    },

    compact: {
      alt: ["cmp"],
      fn: iterate(function (data, _) {
        var _ref35 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref35.multiple;

        return data.replace(/\s+/gm, " ").trim();
      })
    },

    uncomment: {
      fn: iterate(function (data, _) {
        var _ref36 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref36.multiple;

        return data.replace(/<!--|-->/gm, "");
      })
    },

    object: {
      fn: iterate(function (data, _) {
        var _ref37 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref37.multiple;

        var matches = data.match(WIDER_JSON);
        if (!multiple) return matches[0] || null;
        return !(0, _lodash.isEmpty)(matches) ? matches : null;
      })
    },

    phone: {
      alt: ["telephone"],
      fn: iterate(function (data, _) {
        var _ref38 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref38.multiple;

        if (multiple) return data.match(PHONE) || "";
        return data.match(PHONE) !== null ? data.match(PHONE)[0] : "";
      })
    },

    website: {
      alt: ["wbs"],
      fn: iterate(function (data, _) {
        var _ref39 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref39.multiple;

        var websites = data.match(WEBSITE);
        if (multiple) return (0, _lodash.isArray)(websites) ? websites : [websites || ""];
        return websites !== null ? websites[0] : "";
      })
    },

    email: {
      alt: ["mail"],
      fn: iterate(function (data, _) {
        var _ref40 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref40.multiple;

        if (multiple) {
          var emails = data.match(EMAIL) || data;
          if ((0, _lodash.isArray)(emails) && emails.length === 1) {
            return emails[0];
          }
        } else {
          return data.match(EMAIL) !== null ? data.match(EMAIL)[0] : "";
        }
      })
    },

    absoluteUrl: {
      fn: iterate(function (data, _) {
        var _ref41 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref41.multiple;

        var rootUrl = (0, _lodash.get)(this.trunks(), "options.rootUrl", null);
        if (rootUrl) return _url2.default.resolve(rootUrl, data);
        return data;
      })
    },

    html: {
      fn: iterate(function (data, _) {
        return this.html().trim();
      })
    },

    json: {
      fn: iterate(function (data, _) {
        var _ref42 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            multiple = _ref42.multiple;

        if (!multiple) {
          try {
            return JSON.parse((0, _lodash.isArray)(data) ? data[0] : data);
          } catch (error) {
            return null;
          }
        } else if ((0, _lodash.isArray)(data)) {
          return data.map(function (d) {
            try {
              return JSON.parse(d);
            } catch (error) {
              return null;
            }
          });
        }
        return "";
      })
    },

    variable: {
      alt: ["var"],
      fn: iterate(function (data, _ref43) {
        var _ref44 = _slicedToArray(_ref43, 1),
            variablesNames = _ref44[0];

        return extractVarsFromJsCode(data, [variablesNames]);
      })
    },

    match: {
      fn: iterate(function (data, _ref45) {
        var _ref47 = _slicedToArray(_ref45, 2),
            regexModel = _ref47[0],
            regexIndicators = _ref47[1];

        var _ref46 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            _ref46$multiple = _ref46.multiple,
            multiple = _ref46$multiple === undefined ? false : _ref46$multiple;

        if (regexModel) {
          try {
            if (regexModel.startsWith("/")) regexModel = regexModel.slice(1);
            if (regexModel.endsWith("/")) regexModel = regexModel.slice(0, -1);
            var regex = new RegExp(regexModel, regexIndicators || "g");
            var matches = getRegexGroupValues(data, regex);
            console.log("matches", matches);
            if (matches && (0, _lodash.isArray)(matches)) {
              if (multiple) return matches;else return matches[0];
            } else {
              return null;
            }
          } catch (error) {
            throw new Error(error.message);
          }
        }
        throw new Error("no regex model has been passed");
      })
    },

    attribute: {
      alt: ["attr"],
      fn: function fn(data, _ref48) {
        var _ref49 = _slicedToArray(_ref48, 1),
            attribute = _ref49[0];

        var foundAttribute = this.attr(attribute);
        if (!foundAttribute) throw new Error("attribute \"" + attribute + "\" doesn't exists");
        return foundAttribute;
      }
    }
  };
};

var _url = require("url");

var _url2 = _interopRequireDefault(_url);

var _lodash = require("lodash");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = exports["default"];