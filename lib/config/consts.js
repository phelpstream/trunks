"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  MULTIPLE_SYMBOL: "+",
  ACTIONS_DELIMITER: "@",
  NODE_EDITABLEVALUE: "$editableValue",
  FNACTIONS_PREFIX: "$"
};
module.exports = exports["default"];