"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  EMAIL: /([a-zA-Z0-9._-]{1,30}@[a-zA-Z0-9._-]{2,15}\.[a-zA-Z0-9._-]{2,15})/gim,
  PHONE: /(?!\s)\+?\(?\d*\)? ?\(?\d+\)?\d*([\s./-]\d{2,})+/gim,
  WEBSITE: /(?!\s)((https?:\/\/)?(www\.)?[-a-zA-Z0-9:%._\+~#=]{2,256}\.[a-z]{2,6}\b[-a-zA-Z0-9@:%_\+.~#?&/=]*)/gim,
  NUMBERS: /\d+/gm,
  WIDER_PARENTHESIS: /(?:\()(.*)(?:\))/gim,
  MULTI_BRACKETS: /\[([^\[\]]*?)\]/gim,
  WIDER_BRACKETS: /\[(.*)\]/gim,
  WIDER_JSON: /{.+}/gm,
  PARAMETERS_IN_PARENTHESIS: /('.+?')|((?!['\s])[^,]+(?!['\s]))/gim,
  FUNCTIONS: /(\w+(\(.*?\))*\+?)/gm
};
module.exports = exports["default"];