"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _meta = require("./meta");

var _meta2 = _interopRequireDefault(_meta);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  meta: _meta2.default
};
module.exports = exports["default"];