"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.iterate = undefined;

var _lodash = require("lodash");

var _type = require("./type");

var iterate = exports.iterate = function iterate(fn) {
  var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref$onTypes = _ref.onTypes,
      onTypes = _ref$onTypes === undefined ? [] : _ref$onTypes;

  return function (data) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var self = this;
    var rightTypes = (0, _lodash.isEmpty)(onTypes) ? true : ((0, _lodash.isArray)(onTypes) ? onTypes : [onTypes]).every(function (type) {
      return (0, _lodash.isArray)(data) ? data.some(function (d) {
        return (0, _type.isType)(d, type);
      }) : (0, _type.isType)(data, type);
    });
    return (0, _lodash.isArray)(data) && rightTypes ? data.map(function (d) {
      return fn.call.apply(fn, [self, d].concat(args));
    }) : fn.call.apply(fn, [self, data].concat(args));
  };
};

// let addition = (a, b) => a + b

// let iterateAddition = iterate(addition, { onTypes: "number" })

// console.log(iterateAddition(5, 2, {}))
// console.log(iterateAddition([1, 4], 2, {}))