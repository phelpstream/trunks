"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.escape = escape;
exports.unescape = unescape;
exports.getRegexGroupValues = getRegexGroupValues;
exports.functionNameAndParameters = functionNameAndParameters;

var _regex = require("./../config/regex");

function escape(string) {
  return string.replace(/[\/.*+?|[()\]{}\$^-]/g, function ($0) {
    return "\\" + $0;
  });
}

function unescape(string) {
  return string.replace(/\\(.)/g, function ($0, $1) {
    return $1;
  });
}

function getRegexGroupValues(string, regex) {
  var matches = [];
  var found = void 0,
      loopSecurity = string.length;

  try {
    if (string && regex) {
      while ((found = regex.exec(string)) !== null && loopSecurity > 0) {
        loopSecurity--;
        // console.log("found", found)
        matches.push(found[1] || found[0]);
        // regex.lastIndex = found.index + found[found.length - 1].length
      }
    }
  } catch (error) {
    throw new Error("[getRegexGroupValues] string: " + string + ", regex: " + regex + " and: " + error.message);
  } finally {
    return matches;
  }
}

function functionNameAndParameters(functionString) {
  var multipleFnCalls = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  if (functionString.includes("(")) {
    var functionName = functionString.split("(")[0] || null;
    var paramsBlocks = getRegexGroupValues(functionString, _regex.MULTI_PARENTHESIS).map(function (param) {
      return param.includes(",") ? param.split(",") : [param];
    });
    return [functionName, multipleFnCalls ? paramsBlocks : paramsBlocks[0] || null];
  }
  return [null, null];
}