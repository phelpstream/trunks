"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.splitOnLastSeenChar = splitOnLastSeenChar;
exports.extractVarsFromJsCode = extractVarsFromJsCode;
// String.prototype.oneSplitFromEnd = function (char) {
function splitOnLastSeenChar(string, char) {
  var arr = string.split(char),
      res = [];
  res[1] = arr[arr.length - 1];
  arr.pop();
  res[0] = arr.join(char);
  return res;
}

var sideSpacedWord = exports.sideSpacedWord = function sideSpacedWord(word) {
  return " " + word + " ";
};

// let selector = ".class < coool | yes(1) || super"

// console.log(splitOnLastSeenChar(selector, " || "));

function extractVarsFromJsCode(jsStringCode, varsArray) {
  var varsObj = {};
  eval.call(null, jsStringCode);
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = varsArray[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var vars = _step.value;

      try {
        varsObj[vars] = eval(vars) || null;
      } catch (error) {
        throw new Error("variable \"" + vars + "\" doesn't exists");
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return varsObj;
}

var onlyValidText = exports.onlyValidText = function onlyValidText(strings) {
  for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    args[_key - 1] = arguments[_key];
  }

  return strings.reduce(function (result, string, index) {
    return result + string + (args[index] || "");
  }, "");
};