"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var timeSpent = exports.timeSpent = function timeSpent(lastTime) {
  return new Date().getTime() - lastTime;
};