"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var isType = exports.isType = function isType(object, against) {
  if (against.prototype) against = against.prototype.constructor.name;
  var type = Object.prototype.toString.call(object).slice(8, -1).toLowerCase();
  return object !== undefined && object !== null && type === against.toLowerCase();
};