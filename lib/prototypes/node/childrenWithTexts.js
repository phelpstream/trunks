"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

exports.default = function ($) {
	return function childrenWithTexts() {
		return this.contents().filter(function (index, elem) {
			if (elem.type === "text" && elem.data.replace(/\s+/gim, "").length < 1) return false;
			return true;
		});
	};
};

module.exports = exports["default"];