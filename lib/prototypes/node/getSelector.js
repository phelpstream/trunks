"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _templateObject = _taggedTemplateLiteral(["", "", "", "", ""], ["", "", "", "", ""]);

exports.default = function ($) {
  var onlyValidText = $.$trunks.helpers.string.onlyValidText;

  return function getSelector() {
    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref$asObject = _ref.asObject,
        asObject = _ref$asObject === undefined ? false : _ref$asObject;

    var node = $(this).get(0);
    var attributes = Object.assign({}, node.attribs);
    var selector = {
      tag: (0, _lodash.isString)(node.name) ? node.name.toLowerCase() : null,
      id: attributes["id"] || null,
      class: attributes["class"] && !(0, _lodash.isArray)(attributes["class"]) ? [attributes["class"]] : [],
      attributes: function () {
        if (attributes["id"]) delete attributes["id"];
        if (attributes["class"]) delete attributes["class"];
        return attributes;
      }()
    };
    var classes = selector.class.reduce(function (fullClasses, classItem) {
      return fullClasses + "." + classItem;
    }, "") || null;
    var attributesArr = Object.entries(selector.attributes).map(function (_ref2) {
      var _ref3 = _slicedToArray(_ref2, 2),
          key = _ref3[0],
          value = _ref3[1];

      return "[" + key + "=\"" + ((0, _lodash.isArray)(value) ? value.join(" ") : value) + "\"]";
    });
    return asObject ? selector //: onlyValidText`${selector.tag}${selector.id ? `#${selector.id}` : null}${classes}`
    : onlyValidText(_templateObject, selector.tag, selector.id ? "#" + selector.id : null, classes, attributesArr.join(""));
  };
};

var _lodash = require("lodash");

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

module.exports = exports["default"];