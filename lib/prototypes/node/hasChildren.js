"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($) {
  return function hasChildren() {
    return this.children().length > 0 ? this.children() : false;
  };
};

module.exports = exports["default"];