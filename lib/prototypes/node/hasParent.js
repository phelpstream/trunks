"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($) {
  return function hasParent() {
    return this.parent().length > 0 ? this.parent() : false;
  };
};

module.exports = exports["default"];