"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($) {
  return function hasSimilarSiblings() {
    return this.parent().find(this);
  };
};

module.exports = exports["default"];