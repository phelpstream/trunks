"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

exports.default = function ($) {
	return function texts() {
		return this.contents().filter(function (index, elem) {
			if (elem.type === "text" && elem.data.replace(/\s+/gim, "").length > 0) return true;
			return false;
		});
	};
};

module.exports = exports["default"];