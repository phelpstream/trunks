"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($) {
  return function json() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    var stringify = options.stringify || false;
    var tagNameFilters = options.tagNameFilters || ["style", "script"];
    var typeFilters = options.typeFilters || [];

    var outputResult = {};
    var startingNode = $(this);

    var iterateOnTheNode = function iterateOnTheNode(node, output) {
      if (node) {
        $(node).childrenWithTexts().each(function (index, element) {
          var hasChildren = $(this).hasChildren();

          var _$$get = $(this).get(0),
              tagName = _$$get.tagName,
              name = _$$get.name,
              type = _$$get.type,
              attribs = _$$get.attribs;

          var key = (name || type) + "_" + index;
          var text = $(this).text();

          if (tagNameFilters.includes(tagName)) return;
          if (typeFilters.includes(type)) return;

          if (!hasChildren) {
            output[key] = text || attribs || {};
          } else {
            output[key] = {};
            iterateOnTheNode(this, output[key]);
          }
        });
      } else {
        return null;
      }
    };

    iterateOnTheNode(startingNode, outputResult);

    if (stringify) {
      return JSON.stringify(outputResult, null, 2);
    }

    return outputResult;
  };
};

var _lodash = require("lodash");

module.exports = exports["default"];