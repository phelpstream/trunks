"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($) {
  return function parse(frame) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var stringify = options.stringify || false;

    var $trunks = this.trunks();
    var FramePropertyParser = (0, _frameproperty2.default)($trunks);
    var StringParser = (0, _string2.default)($trunks);
    var ArrayParser = (0, _array2.default)($trunks);
    var ObjectParser = (0, _object2.default)($trunks);

    var outputResult = {};
    var startingNode = $(this);

    var setActionsOnIteration = function setActionsOnIteration(obj, output, node) {
      if ((0, _lodash.isObject)(obj)) {
        (0, _lodash.forOwn)(obj, function (value, property) {
          var FrameProperty = FramePropertyParser.isFrameProperty(property);

          if (FrameProperty.isTrue) {
            FramePropertyParser.getData($)(value, FrameProperty.propertyName, $(node), output, setActionsOnIteration);
          } else {
            if (value && (0, _lodash.isString)(value)) {
              // Extract the data from the selector
              var tempResult = StringParser.getData($)(value, $(node));
              output[property] = tempResult;
              // if (tempResult) output[property] = tempResult
            } else if (value && (0, _lodash.isArray)(value)) {
              if (value.length === 1) {
                var _tempResult = ArrayParser.String.getData($)(value, $(node));
                output[property] = _tempResult;
              } else {
                // Concatenate
                var tempResults = [];
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                  for (var _iterator = value[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var unitValue = _step.value;

                    var unitTempResult = StringParser.getData($)(unitValue, $(node));
                    if (unitTempResult) tempResults.push(unitTempResult);
                  }
                } catch (err) {
                  _didIteratorError = true;
                  _iteratorError = err;
                } finally {
                  try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                      _iterator.return();
                    }
                  } finally {
                    if (_didIteratorError) {
                      throw _iteratorError;
                    }
                  }
                }

                output[property] = tempResults.join(' ');
              }
              // if (tempResult) output[property] = tempResult
            } else if (value && (0, _lodash.isObject)(value)) {
              var _tempResult2 = null;
              _tempResult2 = ObjectParser.getData($)(value, property, $(node), output, setActionsOnIteration);
              // output[property] = tempResult
              // if (tempResult) output[property] = tempResult
            }
          }
        });
      } else {
        return null;
      }
    };

    setActionsOnIteration(frame, outputResult, startingNode);

    if (stringify) {
      return JSON.stringify(outputResult, null, 2);
    }

    return outputResult;
  };
};

var _lodash = require("lodash");

var _frameproperty = require("./parse/dom/parsers/frameproperty.parser");

var _frameproperty2 = _interopRequireDefault(_frameproperty);

var _string = require("./parse/dom/parsers/string.parser");

var _string2 = _interopRequireDefault(_string);

var _array = require("./parse/dom/parsers/array.parser");

var _array2 = _interopRequireDefault(_array);

var _object = require("./parse/dom/parsers/object.parser");

var _object2 = _interopRequireDefault(_object);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = exports["default"];