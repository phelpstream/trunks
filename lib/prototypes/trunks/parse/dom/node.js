"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($trunks) {
  var _selector = (0, _selector3.default)($trunks),
      getSelectorActions = _selector.getSelectorActions;

  var _$trunks$consts = $trunks.consts,
      NODE_EDITABLEVALUE = _$trunks$consts.NODE_EDITABLEVALUE,
      FNACTIONS_PREFIX = _$trunks$consts.FNACTIONS_PREFIX;


  var getData = function getData($) {
    return function (node) {
      var actions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      $(node)[NODE_EDITABLEVALUE] = $(node).text();

      if ((0, _lodash.isArray)(actions)) {
        // Detect an image automatically if no attribute in actions queue
        var _Object$assign = Object.assign({ tagName: null, name: null, nodeName: null, localName: null }, $(node).get(0)),
            tagName = _Object$assign.tagName,
            name = _Object$assign.name,
            nodeName = _Object$assign.nodeName,
            localName = _Object$assign.localName;

        var isImageTag = [tagName, name, nodeName, localName].some(function (value) {
          return (0, _lodash.isString)(value) && value.trim().toLowerCase().includes("img");
        });

        // If node is an image
        if (isImageTag) $(node)[NODE_EDITABLEVALUE] = $(node).attr("src");

        // Play actions
        actions.forEach(function (action) {
          var name = action.name,
              params = action.params,
              multiple = action.multiple;

          if (!node || !(0, _lodash.isObject)(node)) console.error("Humm, node isn't really in good shape", node);
          if ($(node)["" + FNACTIONS_PREFIX + name]) {
            var options = { multiple: multiple };
            node = $(node)["" + FNACTIONS_PREFIX + name](params, options);
          }
        });
      }

      return $(node)[NODE_EDITABLEVALUE];
    };
  };

  var getNodes = function getNodes($) {
    return function (selector, node) {
      var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
          _ref$multipleNodes = _ref.multipleNodes,
          multipleNodes = _ref$multipleNodes === undefined ? false : _ref$multipleNodes,
          _ref$nodeIndex = _ref.nodeIndex,
          nodeIndex = _ref$nodeIndex === undefined ? -1 : _ref$nodeIndex,
          _ref$textNodes = _ref.textNodes,
          textNodes = _ref$textNodes === undefined ? false : _ref$textNodes,
          _ref$selfNode = _ref.selfNode,
          selfNode = _ref$selfNode === undefined ? false : _ref$selfNode;

      var nodes = null;

      if (selector.trim().length > 0) {
        nodes = $(node).find(selector);
      } else {
        nodes = node;
      }

      if (textNodes) nodes = $(nodes).texts();

      if (!multipleNodes && nodeIndex !== -1) {
        if ((0, _lodash.isNumber)(+nodeIndex)) {
          nodes = $(nodes).get(+nodeIndex - 1);
        } else if ((0, _lodash.isString)(nodeIndex)) {
          if (nodeIndex.includes("last")) {
            nodes = $(nodes).last();
          }
        }
      } else if (!multipleNodes) {
        nodes = $(nodes).first();
      }

      return nodes;
    };
  };

  var getDataForEachNode = function getDataForEachNode($) {
    return function (selector, node) {
      var _ref2 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
          _ref2$multipleNodes = _ref2.multipleNodes,
          multipleNodes = _ref2$multipleNodes === undefined ? false : _ref2$multipleNodes;

      var _getSelectorActions = getSelectorActions(selector),
          actions = _getSelectorActions.actions,
          cleanSelector = _getSelectorActions.cleanSelector,
          nodeIndex = _getSelectorActions.nodeIndex,
          textNodes = _getSelectorActions.textNodes,
          selfNode = _getSelectorActions.selfNode;

      var data = [];

      var nodes = getNodes($)(cleanSelector, node, { multipleNodes: multipleNodes, nodeIndex: nodeIndex, textNodes: textNodes });

      if (nodes) {
        $(nodes).each(function (i, n) {
          var partialData = getData($)($(n), actions);
          if (partialData) data.push(partialData);
        });
        if ((0, _lodash.isArray)(data) && data.length === 1) data = data[0];
        if (data.length === 0) data = null;
      } else {
        data = null;
      }

      return data;
    };
  };

  return {
    getNodes: getNodes,
    getDataForEachNode: getDataForEachNode
  };
};

var _lodash = require("lodash");

var _selector2 = require("./selector");

var _selector3 = _interopRequireDefault(_selector2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = exports["default"];