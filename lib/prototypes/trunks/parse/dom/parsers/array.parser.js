"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($trunks) {
  var _node = (0, _node3.default)($trunks),
      getDataForEachNode = _node.getDataForEachNode;

  var updateSelectorIfArray = function updateSelectorIfArray(selector) {
    if ((0, _lodash.isArray)(selector)) {
      return {
        ok: true,
        firstSelector: selector[0],
        multipleNodes: true
      };
    }

    return {
      ok: false
    };
  };

  var String = {
    getData: function getData($) {
      return function (selector, node) {
        var _updateSelectorIfArra = updateSelectorIfArray(selector),
            firstSelector = _updateSelectorIfArra.firstSelector,
            multipleNodes = _updateSelectorIfArra.multipleNodes;

        return getDataForEachNode($)(firstSelector, node, { multipleNodes: multipleNodes });
      };
    }
  };

  return { String: String };
};

var _lodash = require("lodash");

var _node2 = require("./../node");

var _node3 = _interopRequireDefault(_node2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = exports["default"];