"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($trunks) {
  var _node = (0, _node3.default)($trunks),
      getNodes = _node.getNodes;

  var isProperty = function isProperty(property) {
    var propertiesList = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _frame2.default;

    var pList = propertiesList,
        // local declaration just in case
    status = false,
        propertyName = null,
        partOfText = false,
        referenceName = null;

    Object.keys(pList).forEach(function (p) {
      pList[p].forEach(function (item) {
        if (item.includes("*")) {
          item = item.replace("*", "");
          partOfText = true;
        }
        if (partOfText && property.includes(item) || item === property) {
          status = true;
          propertyName = p;
          referenceName = property.replace(item, "");
          return true;
        }
      });
    });
    return {
      isTrue: status,
      propertyName: propertyName,
      referenceName: referenceName
    };
  };

  var isFrameProperty = function isFrameProperty(property) {
    return isProperty(property);
  };

  var getPropertyValue = function getPropertyValue(obj, propertyName, propertiesList) {
    var pList = propertiesList;
    var propertyNames = null;
    var result = null;
    var including = false;

    if (pList[propertyName]) {
      propertyNames = pList[propertyName];

      Object.keys(obj).forEach(function (property) {
        propertyNames.forEach(function (name) {
          if (name.includes("*")) {
            name = name.replace("*", "");
            including = true;
          } else {
            including = false;
          }
          if (including && property && property.startsWith(name) || name === property) {
            result = obj[property];
            return true;
          }
        });
      });

      return result;
    } else {
      return false;
    }
  };

  var getFramePropertyValue = function getFramePropertyValue(obj, propertyName) {
    return getPropertyValue(obj, propertyName, _frame2.default);
  };

  var getData = function getData($) {
    return function (value, propertyName, node, output, callback) {
      if (propertyName === "group") {
        var selector = getFramePropertyValue(value, "selector");
        var data = getFramePropertyValue(value, "data");
        var multiple = false;
        if ((0, _lodash.isArray)(data)) {
          multiple = true;
        }

        if (selector && data) {
          if ((0, _lodash.isString)(selector)) {
            var nextNode = getNodes($)(selector, node, {
              multiple: multiple
            });
            callback(data, output, nextNode);
          }
        }
      }
    };
  };

  return {
    getData: getData,
    getFramePropertyValue: getFramePropertyValue,
    getPropertyValue: getPropertyValue,
    isFrameProperty: isFrameProperty,
    isProperty: isProperty
  };
};

var _lodash = require("lodash");

var _node2 = require("./../node");

var _node3 = _interopRequireDefault(_node2);

var _frame = require("./../../frame.properties");

var _frame2 = _interopRequireDefault(_frame);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = exports["default"];