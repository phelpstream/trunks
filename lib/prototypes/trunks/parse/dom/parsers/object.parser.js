"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($trunks) {
  var FramePropertyParser = (0, _frameproperty2.default)($trunks);
  var StringParser = (0, _string2.default)($trunks);
  var ArrayParser = (0, _array2.default)($trunks);

  var _node = (0, _node3.default)($trunks),
      getNodes = _node.getNodes;

  var getData = function getData($) {
    return function (value, property, node, output, callback) {
      var containsSelector = FramePropertyParser.getFramePropertyValue(value, "selector");
      var containsData = FramePropertyParser.getFramePropertyValue(value, "data");
      var containsActions = FramePropertyParser.getFramePropertyValue(value, "actions");

      var multipleNodes = false;
      if ((0, _lodash.isArray)(containsData) && !((0, _lodash.isArray)(containsSelector) && containsSelector.length > 1)) {
        multipleNodes = true;
        containsData = containsData[0];
      }
      // Contains Selector as an Array
      if (containsSelector && (0, _lodash.isArray)(containsSelector)) {
        if (containsSelector.length === 1) {
          var tempResult = ArrayParser.String.getData($)(containsSelector, $(node));
          output[property] = tempResult;
        } else {
          // Concatenate
          var tempResults = [];
          var _iteratorNormalCompletion = true;
          var _didIteratorError = false;
          var _iteratorError = undefined;

          try {
            for (var _iterator = containsSelector[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              var unitValue = _step.value;

              var unitTempResult = StringParser.getData($)(unitValue, $(node));
              if (unitTempResult) tempResults.push(unitTempResult);
            }
          } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
              }
            } finally {
              if (_didIteratorError) {
                throw _iteratorError;
              }
            }
          }

          output[property] = tempResults.join(" ");
        }
        // Contains Selector && Contains Data
      } else if (containsSelector && (0, _lodash.isString)(containsSelector) && containsData) {
        if ((0, _lodash.isString)(containsData)) {
          var nextNode = getNodes($)(containsSelector, $(node), {
            multipleNodes: multipleNodes
          });
          return StringParser.getData($)(containsData, $(nextNode));
        } else if ((0, _lodash.isObject)(containsData)) {
          var _nextNode = getNodes($)(containsSelector, node, {
            multipleNodes: multipleNodes
          });

          if (multipleNodes) {
            output[property] = [];
            $(_nextNode).each(function (index, n) {
              output[property][index] = {};
              callback(containsData, output[property][index], $(n));
            });
          } else {
            output[property] = {};
            callback(containsData, output[property], _nextNode);
          }
        }
        // Contains Selector as a String
      } else if (containsSelector && (0, _lodash.isString)(containsSelector)) {
        return StringParser.getData($)(containsSelector, $(node));
        if ((0, _lodash.isString)(containsSelector)) {} else {}

        // logger.debug(sep)
        // Pass on the object for organization
      } else if (!containsSelector) {
        output[property] = {};
        callback(value, output[property], node);
      }
    };
  };
  return { getData: getData };
};

var _lodash = require("lodash");

var _node2 = require("./../node");

var _node3 = _interopRequireDefault(_node2);

var _frameproperty = require("./frameproperty.parser");

var _frameproperty2 = _interopRequireDefault(_frameproperty);

var _string = require("./string.parser");

var _string2 = _interopRequireDefault(_string);

var _array = require("./array.parser");

var _array2 = _interopRequireDefault(_array);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * All the functions required to parse Object containsData
 */

module.exports = exports["default"];