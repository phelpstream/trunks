"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($trunks) {
  var _node = (0, _node3.default)($trunks),
      getDataForEachNode = _node.getDataForEachNode;

  var getData = function getData($) {
    return function (selector, node) {
      return getDataForEachNode($)(selector, node);
    };
  };
  return { getData: getData };
};

var _node2 = require("./../node");

var _node3 = _interopRequireDefault(_node2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * All the functions required to parse String data
 */

module.exports = exports["default"];