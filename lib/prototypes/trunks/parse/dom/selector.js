"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($trunks) {
  var _$trunks$helpers$rege = $trunks.helpers.regex,
      getRegexGroupValues = _$trunks$helpers$rege.getRegexGroupValues,
      unescape = _$trunks$helpers$rege.unescape;
  var _$trunks$helpers$stri = $trunks.helpers.string,
      splitOnLastSeenChar = _$trunks$helpers$stri.splitOnLastSeenChar,
      sideSpacedWord = _$trunks$helpers$stri.sideSpacedWord;
  var _$trunks$consts = $trunks.consts,
      MULTIPLE_SYMBOL = _$trunks$consts.MULTIPLE_SYMBOL,
      ACTIONS_DELIMITER = _$trunks$consts.ACTIONS_DELIMITER;
  var _$trunks$consts$regex = $trunks.consts.regexes,
      MULTI_BRACKETS = _$trunks$consts$regex.MULTI_BRACKETS,
      FUNCTIONS = _$trunks$consts$regex.FUNCTIONS,
      PARAMETERS_IN_PARENTHESIS = _$trunks$consts$regex.PARAMETERS_IN_PARENTHESIS,
      WIDER_PARENTHESIS = _$trunks$consts$regex.WIDER_PARENTHESIS;


  var getNodeIndexFromSelector = function getNodeIndexFromSelector(selector) {
    var indexMatches = selector.trim().match(/\s\d+$/gm);
    if (indexMatches && indexMatches[0]) return {
      nodeIndex: +indexMatches[0] || 1,
      cleanSelector: selector.trim().slice(0, -indexMatches[0].length).trim()
    };
    return null;
  };

  var getTextNodesFromSelector = function getTextNodesFromSelector(selector) {
    if (selector && selector.trim().endsWith("$text")) return {
      textNodes: true,
      cleanSelector: selector.replace("$text", "")
    };
    return null;
  };

  var getSelectorActions = function getSelectorActions(selector) {
    var result = {
      cleanSelector: selector,
      // selfNode: false,
      actions: [],
      nodeIndex: 1,
      textNodes: false

      // console.log("selector", selector)

    };if (selector && selector.includes(ACTIONS_DELIMITER)) {
      result.cleanSelector = selector.split(ACTIONS_DELIMITER)[0].trim();
      var actionsString = selector.split(ACTIONS_DELIMITER)[1].trim();
      var actionsArr = actionsString.match(FUNCTIONS);

      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = actionsArr[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var action = _step.value;

          var betweenParanthesis = getRegexGroupValues(action, WIDER_PARENTHESIS)[0] || null;
          var actionParams = betweenParanthesis ? getRegexGroupValues(betweenParanthesis, PARAMETERS_IN_PARENTHESIS) : null;
          if (actionParams) actionParams = actionParams.map(function (actionParam) {
            if (actionParam.startsWith("'")) actionParam = actionParam.slice(1);
            if (actionParam.endsWith("'")) actionParam = actionParam.slice(0, -1);
            return actionParam;
          });

          var actionToPush = {
            name: actionParams ? action.split("(")[0] : action,
            params: actionParams,
            multiple: action.endsWith(MULTIPLE_SYMBOL)
          };
          if (actionToPush.multiple) actionToPush.name.slice(0, -1);
          result.actions.push(actionToPush);
        }
        // result.actions.reverse()
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    }

    var checkNodeIndexInSelector = getNodeIndexFromSelector(result.cleanSelector);
    if (checkNodeIndexInSelector) {
      result.cleanSelector = checkNodeIndexInSelector.cleanSelector;
      result.nodeIndex = checkNodeIndexInSelector.nodeIndex;
    }

    var checkTextNodesInSelector = getTextNodesFromSelector(result.cleanSelector);
    if (checkTextNodesInSelector) {
      result.cleanSelector = checkTextNodesInSelector.cleanSelector;
      result.textNodes = checkTextNodesInSelector.textNodes;
    }

    // if (selfnode.some(selfnodeDeclaration => result.cleanSelector.trim().startsWith(selfnodeDeclaration))) {
    //   result.selfNode = true
    //   selfnode.forEach(function(selfnodeDeclaration) {
    //     if (result.cleanSelector.includes(selfnodeDeclaration)) result.cleanSelector = result.cleanSelector.replace(selfnodeDeclaration, "")
    //   })
    // }

    return result;
  };

  return { getSelectorActions: getSelectorActions };
};

var _lodash = require("lodash");

module.exports = exports["default"];
// import { selfnode } from "./../selector.properties"