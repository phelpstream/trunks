"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  selector: ["_s", "_selector", "$s", "$selector"],
  data: ["_d", "_data", "$d", "$data"],
  actions: ["_a", "_actions", "$a", "$actions"],
  group: ["_g*", "_group*", "$g*", "$group*"]
};
module.exports = exports["default"];