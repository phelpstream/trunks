"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($) {
  return function () {
    return this.parse.apply(this, arguments);
  };
};

module.exports = exports["default"];