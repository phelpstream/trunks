'use strict';

var _trunks = require('./trunks');

var _trunks2 = _interopRequireDefault(_trunks);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.trunks = _trunks2.default;
global.trunks = _trunks2.default;