"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _lodash = require("lodash");

var _cheerio = require("cheerio");

var _cheerio2 = _interopRequireDefault(_cheerio);

var _addPrototypes = require("./functions/addPrototypes");

var _addPrototypes2 = _interopRequireDefault(_addPrototypes);

var _texts = require("./prototypes/node/texts");

var _texts2 = _interopRequireDefault(_texts);

var _childrenWithTexts = require("./prototypes/node/childrenWithTexts");

var _childrenWithTexts2 = _interopRequireDefault(_childrenWithTexts);

var _hasChildren = require("./prototypes/node/hasChildren");

var _hasChildren2 = _interopRequireDefault(_hasChildren);

var _hasParent = require("./prototypes/node/hasParent");

var _hasParent2 = _interopRequireDefault(_hasParent);

var _hasSimilarSiblings = require("./prototypes/node/hasSimilarSiblings");

var _hasSimilarSiblings2 = _interopRequireDefault(_hasSimilarSiblings);

var _getSelector = require("./prototypes/node/getSelector");

var _getSelector2 = _interopRequireDefault(_getSelector);

var _parse = require("./prototypes/trunks/parse");

var _parse2 = _interopRequireDefault(_parse);

var _scrape = require("./prototypes/trunks/scrape");

var _scrape2 = _interopRequireDefault(_scrape);

var _json = require("./prototypes/trunks/json");

var _json2 = _interopRequireDefault(_json);

var _trunks = require("./prototypes/trunks/trunks");

var _trunks2 = _interopRequireDefault(_trunks);

var _actions = require("./actions/actions");

var _actions2 = _interopRequireDefault(_actions);

var _default = require("./actions/default");

var _default2 = _interopRequireDefault(_default);

var _regex = require("./config/regex");

var _regex2 = _interopRequireDefault(_regex);

var _consts = require("./config/consts");

var _consts2 = _interopRequireDefault(_consts);

var _regex3 = require("./helpers/regex");

var regexHelpers = _interopRequireWildcard(_regex3);

var _string = require("./helpers/string");

var stringHelpers = _interopRequireWildcard(_string);

var _iterate = require("./helpers/iterate");

var iterateHelpers = _interopRequireWildcard(_iterate);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Trunks - custom implementation of cheerio
 * @constructor
 * @param {(string|Object)} input - Can be HTML or the $ cheerio itself
 * @param {Object} options - Trunks' options
 * @param {Object} cheerioOptions - Cheerio's options
 */

function trunks() {
  var input = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var trunksOptions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var cheerioOptions = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  if (input === "") throw Error("No input detected!");

  var $ = {};
  if ((0, _lodash.isObject)(input)) $ = input;else if ((0, _lodash.isString)(input)) $ = _cheerio2.default.load(input, cheerioOptions);

  $.$trunks = {
    options: (0, _lodash.isObject)(trunksOptions) ? trunksOptions : {},
    load: function load() {
      var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          _ref$node = _ref.node,
          node = _ref$node === undefined ? null : _ref$node,
          _ref$actions = _ref.actions,
          actions = _ref$actions === undefined ? null : _ref$actions;

      if (node) (0, _addPrototypes2.default)($)(node);
      if (actions) _actions2.default.load($)(actions);
    },
    consts: Object.assign(_consts2.default, { regexes: _regex2.default }),
    helpers: {
      regex: regexHelpers,
      string: stringHelpers,
      iterate: iterateHelpers
    }

    // Load cheerio extension
  };$.$trunks.load({
    node: {
      texts: _texts2.default,
      childrenWithTexts: _childrenWithTexts2.default,
      hasChildren: _hasChildren2.default,
      hasParent: _hasParent2.default,
      hasSimilarSiblings: _hasSimilarSiblings2.default,
      getSelector: _getSelector2.default
    },
    actions: [_default2.default]
  });

  // Load trunks prototypes
  $.$trunks.load({
    node: {
      trunks: _trunks2.default,
      parse: _parse2.default,
      scrape: _scrape2.default,
      json: _json2.default
    }
  });

  return $;
}

// helpers


// consts


// default actions


// default prototypes


// cheerios prototypes helpers
exports.default = trunks;
module.exports = exports["default"];