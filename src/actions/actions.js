import { isObject, isArray, isFunction, curry } from "lodash"
import { functionParameters } from "./../helpers/regex"
import { NODE_EDITABLEVALUE, FNACTIONS_PREFIX } from "./../config/consts"

export function Actions() {
  if (!(this instanceof Actions)) {
    return new Actions(...arguments)
  }

  let self = this

  this.actions = {}

  this.add = function($) {
    return function(actionsObj) {
      let actionsObjList = actionsObj
      if (isObject(actionsObj) && !isArray(actionsObj)) actionsObjList = [actionsObj]
      for (let actionsObj of actionsObjList) {
        if (isFunction(actionsObj)) actionsObj = actionsObj($.$trunks)
        if (isObject(actionsObj)) self.actions = Object.assign(actionsObj, self.actions)
      }
      return self.actions
    }
  }

  this.load = function($) {
    return function(actionsObj) {
      let actions = self.add($)(actionsObj)
      for (let [actionName, actionObj] of Object.entries(actions)) {
        if (actionObj && actionObj.fn && isFunction(actionObj.fn)) {
          $.prototype[`${FNACTIONS_PREFIX}${actionName}`] = function(...args) {
            this.toValue = this.toString = this.$value = this.$val = () => this[NODE_EDITABLEVALUE]
            if (!this[NODE_EDITABLEVALUE]) this[NODE_EDITABLEVALUE] = this.text() || ""
            try {
              this[NODE_EDITABLEVALUE] = actionObj.fn.call(this, this[NODE_EDITABLEVALUE], ...args)
            } catch (error) {
              // console.error(`[Error on action fn: ${actionName}] ${error.message}`)
            } finally {
              return this
            }
          }

          if (actionObj.alt && isArray(actionObj.alt)) {
            for (let alternativeActionName of actionObj.alt) {
              $.prototype[`${FNACTIONS_PREFIX}${alternativeActionName}`] = $.prototype[`${FNACTIONS_PREFIX}${actionName}`]
            }
          }
        }
      }
    }
  }

  // this.play = function(data, actionValue, options = {}) {
  //   if (actionValue && actionValue !== "") {
  //     let [actionName, actionParams] = functionNameAndParameters(actionValue)
  //     if (actionName) {
  //       for (let actionKey in self.actions) {
  //         let keywords = [actionKey]
  //         let alternativeKeywords = self.actions[actionKey].alt || []
  //         keywords.push(...alternativeKeywords)

  //         if (keywords.some(keyword => actionValue.includes(keyword))) {
  //           let allOptions = Object.assign({ value: actionValue }, options)
  //           return self.actions[actionKey].fn(data, allOptions)
  //         }
  //       }
  //     }
  //   }
  //   return data
  // }
}

export default new Actions()
