import url from "url"
import { get, isArray, isEmpty } from "lodash"

export default function($trunks) {
  let { functionParameters, getRegexGroupValues } = $trunks.helpers.regex
  let { extractVarsFromJsCode } = $trunks.helpers.string
  let { iterate } = $trunks.helpers.iterate
  let { MULTI_PARENTHESIS, WIDER_PARENTHESIS, WIDER_JSON, NUMBERS, EMAIL, PHONE, WEBSITE } = $trunks.consts.regexes

  return {
    // condition: {
    //   alt: ['if'],
    //   fn: iterate((data, [condition], { multiple } = {}) => {
    //     let result = (function(data, condition){
    //       return eval(`${data}${condition}`)
    //     })()
    //   })
    // },

    split: {
      fn: iterate((data, [stringSplitter], { multiple } = {}) => {
        let splitData
        if (stringSplitter && data && data.includes(stringSplitter)) {
          splitData = data.split(stringSplitter)
        } else if (data.includes(" ")) {
          splitData = data.split(" ")
        } else {
          throw new Error("no data to split")
        }
        return splitData.map(x => x.trim()).filter(x => x !== "")
      })
    },

    between: {
      fn: iterate((data, [betweenA, andB], { multiple } = {}) => {
        if (betweenA && andB) {
          return data
            .split(betweenA)
            .pop()
            .split(andB)
            .shift()
        }
        return data
      })
    },

    after: {
      fn: iterate((data, [afterString], { multiple } = {}) => (afterString ? data.split(afterString).pop() : data))
    },

    before: {
      fn: iterate((data, [beforeString], { multiple, value } = {}) => (afterString ? data.split(afterString).shift() : data))
    },

    trim: {
      fn: iterate((data, _, { multiple } = {}) => (isString(data) ? data.trim() : null))
    },

    join: {
      fn: iterate(
        (data, [joinerString], { multiple } = {}) => {
          if (isArray(data)) {
            if (joinerString) return data.join(joinerString)
            return data.join(" ")
          }
          return data
        },
        {
          onTypes: ["array"]
        }
      )
    },

    lowercase: {
      alt: ["lcase"],
      fn: iterate((data, _, { multiple = false } = {}) => data.toLowerCase())
    },

    uppercase: {
      alt: ["ucase"],
      fn: iterate((data, _, { multiple = false } = {}) => data.toUpperCase())
    },

    number: {
      alt: ["nb"],
      fn: iterate((data, _, { multiple } = {}) => {
        if (multiple) return data.match(NUMBERS) || ""
        return data.match(NUMBERS) !== null ? data.match(NUMBERS)[0] : ""
      })
    },

    numbers: {
      alt: ["nbs"],
      fn: iterate((data, _, { multiple } = {}) => {
        let numbers = data.match(NUMBERS)
        if (numbers !== null) return numbers.join(" ")
        return null
      })
    },

    words: {
      alt: ["w"],
      fn: iterate((data, _, { multiple } = {}) => data.replace(NUMBERS, " "))
    },

    noescapchar: {
      alt: ["nec"],
      fn: iterate((data, _, { multiple } = {}) => data.replace(/\t+|\n+|\r+/gm, " "))
    },

    right: {
      fn: iterate(
        (data, [index], { multiple } = {}) => {
          if (Array.isArray(data)) return data.slice(data.length - index, data.length)
          return data.substr(data.length - index)
        },
        { onTypes: ["array", "string"] }
      )
    },

    left: {
      fn: iterate(
        (data, [index], { multiple } = {}) => {
          if (Array.isArray(data)) return data.slice(0, index)
          return data.substr(0, index)
        },
        { onTypes: ["array", "string"] }
      )
    },

    fromto: {
      fn: iterate(
        (data, [fromIndex, toIndex], { multiple } = {}) => {
          if (Array.isArray(data)) return data.slice(fromIndex, toIndex + 1)
          return data.substr(fromIndex, toIndex)
        },
        { onTypes: ["array", "string"] }
      )
    },

    get: {
      fn: iterate(
        (data, [index], { multiple } = {}) => {
          if (Array.isArray(data) && data[nb - 1]) return data[nb - 1]
          return data.charAt(nb)
        },
        { onTypes: ["array", "string"] }
      )
    },

    compact: {
      alt: ["cmp"],
      fn: iterate((data, _, { multiple } = {}) => data.replace(/\s+/gm, " ").trim())
    },

    uncomment: {
      fn: iterate((data, _, { multiple } = {}) => data.replace(/<!--|-->/gm, ""))
    },

    object: {
      fn: iterate((data, _, { multiple } = {}) => {
        let matches = data.match(WIDER_JSON)
        if (!multiple) return matches[0] || null
        return !isEmpty(matches) ? matches : null
      })
    },

    phone: {
      alt: ["telephone"],
      fn: iterate((data, _, { multiple } = {}) => {
        if (multiple) return data.match(PHONE) || ""
        return data.match(PHONE) !== null ? data.match(PHONE)[0] : ""
      })
    },

    website: {
      alt: ["wbs"],
      fn: iterate((data, _, { multiple } = {}) => {
        let websites = data.match(WEBSITE)
        if (multiple) return isArray(websites) ? websites : [websites || ""]
        return websites !== null ? websites[0] : ""
      })
    },

    email: {
      alt: ["mail"],
      fn: iterate((data, _, { multiple } = {}) => {
        if (multiple) {
          let emails = data.match(EMAIL) || data
          if (isArray(emails) && emails.length === 1) {
            return emails[0]
          }
        } else {
          return data.match(EMAIL) !== null ? data.match(EMAIL)[0] : ""
        }
      })
    },

    absoluteUrl: {
      fn: iterate(function(data, _, { multiple } = {}) {
        let rootUrl = get(this.trunks(), "options.rootUrl", null)
        if (rootUrl) return url.resolve(rootUrl, data)
        return data
      })
    },

    html: {
      fn: iterate(function(data, _) {
        return this.html().trim()
      })
    },

    json: {
      fn: iterate((data, _, { multiple } = {}) => {
        if (!multiple) {
          try {
            return JSON.parse(isArray(data) ? data[0] : data)
          } catch (error) {
            return null
          }
        } else if (isArray(data)) {
          return data.map(d => {
            try {
              return JSON.parse(d)
            } catch (error) {
              return null
            }
          })
        }
        return ""
      })
    },

    variable: {
      alt: ["var"],
      fn: iterate((data, [variablesNames]) => extractVarsFromJsCode(data, [variablesNames]))
    },

    match: {
      fn: iterate((data, [regexModel, regexIndicators], { multiple = false } = {}) => {
        if (regexModel) {
          try {
            if (regexModel.startsWith("/")) regexModel = regexModel.slice(1)
            if (regexModel.endsWith("/")) regexModel = regexModel.slice(0, -1)
            let regex = new RegExp(regexModel, regexIndicators || "g")
            let matches = getRegexGroupValues(data, regex)
            console.log("matches", matches)
            if (matches && isArray(matches)) {
              if (multiple) return matches
              else return matches[0]
            } else {
              return null
            }
          } catch (error) {
            throw new Error(error.message)
          }
        }
        throw new Error("no regex model has been passed")
      })
    },

    attribute: {
      alt: ["attr"],
      fn: function(data, [attribute]) {
        let foundAttribute = this.attr(attribute)
        if (!foundAttribute) throw new Error(`attribute "${attribute}" doesn't exists`)
        return foundAttribute
      }
    }
  }
}
