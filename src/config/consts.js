export default {
  MULTIPLE_SYMBOL: "+",
  ACTIONS_DELIMITER: "@",
  NODE_EDITABLEVALUE: "$editableValue",
  FNACTIONS_PREFIX: "$"
}
