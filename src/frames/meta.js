export default {
  $g_head: {
    $s: "head",
    $d: {
      title: "title",
      base: "base @ attr(href)",
      lang: "@ attr(lang)",
      meta: {
        // Name of web application (only should be used if the website is used as an app)
        applicationName: 'meta[name="application-name"] @ attr(content)',
        // Short description of the page (limit to 150 characters)
        // In *some* situations this description is used as a part of the snippet shown in the search results.
        description: "meta[name=description] @ attr(content)",
        // Short description of your site's subject
        subject: "meta[name=subject] @ attr(content)",
        keywords: "meta[name=keywords] @ attr(content)",
        copyright: "meta[name=copyright] @ attr(content)",
        language: "meta[name=language] @ attr(content)",
        // set character encoding for the document
        charset: "meta[name=charset] @ attr(charset)",
        // Control the behavior of search engine crawling and indexing
        robots: "meta[name=robots] @ attr(content)",
        canonical: "link[rel=canonical] @ attr(content)",
        revised: "meta[name=revised] @ attr(content)",
        // Very short (10 words or less) description. Primarily for academic papers
        abstract: "meta[name=abstract] @ attr(content)",
        topic: "meta[name=topic] @ attr(content)",
        summary: "meta[name=summary] @ attr(content)",
        classification: "meta[name=Classification] @ attr(content)",
        author: "meta[name=author] @ attr(content)",
        // Used to name software used to build the website (i.e. - WordPress, Dreamweaver)
        generator: "meta[name=generator] @ attr(content)",
        // Full domain name or web address
        url: "meta[name=url] @ attr(content)",
        directory: "meta[name=directory] @ attr(content)",
        // Allows control over how referrer information is passed
        referrer: "meta[name=referrer] @ attr(content)",
        // Disable automatic detection and formatting of possible phone numbers
        "format-detection": 'meta[name="format-detection"] @ attr(content)',
        // Gives a general age rating based on sites content
        rating: "meta[name=rating] @ attr(content)",
        // Completely opt out of DNS prefetching by setting to 'off'
        dnsFetchControl: 'meta[http-equiv="x-dns-prefetch-control"] @ attr(content)',
        preloading: {
          dnsPrefetch: ['link[rel="dns-prefetch"] @ attr(href)'],
          preconnect: ["link[rel=preconnect] @ attr(href)"],
          prefetch: ["link[rel=prefetch] @ attr(href)"],
          prerender: ["link[rel=prerender] @ attr(href)"],
          preload: ["link[rel=preload] @ attr(href)"]
        },
        // Stores cookie on the client web browser for client identification
        setCookie: 'meta[http-equiv="set-cookie"] @ attr(content)',
        // Specifies the page to appear in a specific frame
        windowTarget: 'meta[http-equiv="Window-Target"] @ attr(content)',
        favicons: {
          $s: "link[rel*=icon]",
          $d: [
            {
              href: " @ attr(href)",
              type: " @ attr(type)",
              sizes: " @ attr(sizes)"
            }
          ]
        },
        alternates: {
          $s: "link[rel=alternate]",
          $d: [
            {
              link: "@ attr(href)",
              lang: "@ attr(lang)"
            }
          ]
        },
        geolocation: {
          ICBM: "meta[name=ICBM] @ attr(content)",
          geoPosition: 'meta[name="geo.position"] @ attr(content)',
          geoRegion: 'meta[name="geo.region"] @ attr(content)',
          geoPlacename: 'meta[name="geo.placename"] @ attr(content)'
        },
        google: {
          publisher: "link[rel=publisher] @ attr(href)",
          name: "meta[itemprop=name] @ attr(content)",
          description: "meta[itemprop=description] @ attr(content)",
          image: "meta[itemprop=image] @ attr(content)",
          googlebot: "meta[name=googlebot] @ attr(content)",
          // Tells Google stuffs like "nositelinkssearchbox" or "notranslate""
          google: "meta[name=google] @ attr(content)",
          // Verify ownership for Google Search Console
          "google-site-verification": 'meta[name="google-site-verification"] @ attr(content)'
        },
        twitter: {
          card: 'meta[property="twitter:card"] @ attr(content)',
          title: 'meta[property="twitter:title"] @ attr(content)',
          description: 'meta[property="twitter:description"] @ attr(content)',
          creator: 'meta[property="twitter:creator"] @ attr(content)',
          image: 'meta[property="twitter:image"] @ attr(content)',
          url: 'meta[property="twitter:url"] @ attr(content)',
          site: 'meta[property="twitter:site"] @ attr(content)'
        },
        // More at https://developers.facebook.com/docs/sharing/opengraph/object-properties#standard
        facebook: {
          appId: 'meta[property="fb:app_id"] @ attr(content)',
          title: 'meta[property="og:title"] @ attr(content)',
          type: 'meta[property="og:type"] @ attr(content)',
          url: 'meta[property="og:url"] @ attr(content)',
          image: 'meta[property="og:image"] @ attr(content)',
          video: 'meta[property="og:video"] @ attr(content)',
          determiner: 'meta[property="og:determiner"] @ attr(content)',
          description: 'meta[property="og:description"] @ attr(content)',
          locale: 'meta[property="og:locale"] @ attr(content)',
          updatedTime: 'meta[property="og:updated_time"] @ attr(content)',
          seeAlso: 'meta[property="og:see_also"] @ attr(content)',
          richAttachment: 'meta[property="og:rich_attachment"] @ attr(content)',
          ttl: 'meta[property="og:ttl"] @ attr(content)',
          restrictions: {
            age: 'meta[property="og:restrictions:age"] @ attr(content)',
            country: 'meta[property="og:restrictions:country"] @ attr(content)',
            content: 'meta[property="og:restrictions:content"] @ attr(content)'
          },
          author: 'meta[property="article:author"] @ attr(content)',
          profileId: 'meta[property="fb:profile_id"] @ attr(content)',
          admins: 'meta[property="fb:admins"] @ attr(content)',
          siteName: 'meta[property="og:site_name"] @ attr(content)'
        },
        pinterest: {
          pinterest: "meta[name=pinterest] @ attr(content)"
        },
        app: {
          ios: {
            url: 'meta[property="al:ios:url"] @ attr(content)',
            appStoreId: 'meta[property="al:ios:app$store_id"] @ attr(content)',
            appName: 'meta[property="al:ios:app_name"] @ attr(content)'
          },
          android: {
            url: 'meta[property="al:android:url"] @ attr(content)',
            appName: 'meta[property="al:android:app_name"] @ attr(content)',
            package: 'meta[property="al:android:package"] @ attr(content)'
          },
          web: {
            url: 'meta[property="al:web:url"] @ attr(content)'
          }
        }
      }
    }
  },
  $g_body: {
    $s: "body",
    $d: {
      context: {
        sublinks: {
          $s: 'a:not([href^="#"])',
          $d: [
            {
              name: "@ attr(name)",
              url: "@ attr(href)",
              rel: "@ attr(rel)",
              media: "@ attr(media)",
              target: "@ attr(target)",
              type: "@ attr(type)"
            }
          ]
        }
      }
    }
  },
  $g_all: {
    $s: "body",
    $d: {
      emails: ["@ html email"],
      phones: ["@ html phone"]
    }
  }
}
