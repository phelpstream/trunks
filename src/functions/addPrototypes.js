import { isObject, isFunction } from "lodash"

export default function($) {
  return function(prototypesLiteral) {
    if (isObject(prototypesLiteral)) {
      for (let [name, fn] of Object.entries(prototypesLiteral)) {
        if (isFunction(fn)) {
          $.prototype[name] = fn($)
        }
      }
    }
  }
}
