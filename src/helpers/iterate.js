import { isArray, isEmpty } from "lodash"
import { isType } from "./type"

export let iterate = function(fn, { onTypes = [] } = {}) {
  return function(data, ...args) {
    let self = this
    let rightTypes = isEmpty(onTypes)
      ? true
      : (isArray(onTypes) ? onTypes : [onTypes]).every(type => (isArray(data) ? data.some(d => isType(d, type)) : isType(data, type)))
    return isArray(data) && rightTypes ? data.map(d => fn.call(self, d, ...args)) : fn.call(self, data, ...args)
  }
}

// let addition = (a, b) => a + b

// let iterateAddition = iterate(addition, { onTypes: "number" })

// console.log(iterateAddition(5, 2, {}))
// console.log(iterateAddition([1, 4], 2, {}))
