import { MULTI_PARENTHESIS } from "./../config/regex"

export function escape(string) {
  return string.replace(/[\/.*+?|[()\]{}\$^-]/g, function($0) {
    return "\\" + $0
  })
}

export function unescape(string) {
  return string.replace(/\\(.)/g, function($0, $1) {
    return $1
  })
}

export function getRegexGroupValues(string, regex) {
  let matches = []
  let found,
    loopSecurity = string.length

  try {
    if (string && regex) {
      while ((found = regex.exec(string)) !== null && loopSecurity > 0) {
        loopSecurity--
        // console.log("found", found)
        matches.push(found[1] || found[0])
        // regex.lastIndex = found.index + found[found.length - 1].length
      }
    }
  } catch (error) {
    throw new Error(`[getRegexGroupValues] string: ${string}, regex: ${regex} and: ${error.message}`)
  } finally {
    return matches
  }
}

export function functionNameAndParameters(functionString, multipleFnCalls = false) {
  if (functionString.includes("(")) {
    let functionName = functionString.split("(")[0] || null
    let paramsBlocks = getRegexGroupValues(functionString, MULTI_PARENTHESIS).map(param => (param.includes(",") ? param.split(",") : [param]))
    return [functionName, multipleFnCalls ? paramsBlocks : paramsBlocks[0] || null]
  }
  return [null, null]
}
