// String.prototype.oneSplitFromEnd = function (char) {
export function splitOnLastSeenChar(string, char) {
  let arr = string.split(char),
    res = []
  res[1] = arr[arr.length - 1]
  arr.pop()
  res[0] = arr.join(char)
  return res
}

export let sideSpacedWord = word => ` ${word} `

// let selector = ".class < coool | yes(1) || super"

// console.log(splitOnLastSeenChar(selector, " || "));

export function extractVarsFromJsCode(jsStringCode, varsArray) {
  let varsObj = {}
  eval.call(null, jsStringCode)
  for (let vars of varsArray) {
    try {
      varsObj[vars] = eval(vars) || null
    } catch (error) {
      throw new Error(`variable "${vars}" doesn't exists`)
    }
  }
  return varsObj
}

export let onlyValidText = (strings, ...args) => strings.reduce((result, string, index) => result + string + (args[index] || ""), "")
