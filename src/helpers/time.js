export const timeSpent = function(lastTime) {
  return new Date().getTime() - lastTime
}
