export default function($){
	return function childrenWithTexts(){
		return this.contents().filter((index, elem) => {
			if (elem.type === "text" && elem.data.replace(/\s+/gim, "").length < 1) return false
			return true
		})
	}
}