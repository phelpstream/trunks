import { isArray, isString } from "lodash"

export default function($) {
  const { onlyValidText } = $.$trunks.helpers.string
  return function getSelector({ asObject = false } = {}) {
    let node = $(this).get(0)
    let attributes = Object.assign({}, node.attribs)
    let selector = {
      tag: isString(node.name) ? node.name.toLowerCase() : null,
      id: attributes["id"] || null,
      class: attributes["class"] && !isArray(attributes["class"]) ? [attributes["class"]] : [],
      attributes: (function() {
        if (attributes["id"]) delete attributes["id"]
        if (attributes["class"]) delete attributes["class"]
        return attributes
      })()
    }
    let classes = selector.class.reduce((fullClasses, classItem) => `${fullClasses}.${classItem}`, "") || null
    let attributesArr = Object.entries(selector.attributes).map(([key, value]) => `[${key}="${isArray(value) ? value.join(" ") : value}"]`)
    return asObject
      ? selector //: onlyValidText`${selector.tag}${selector.id ? `#${selector.id}` : null}${classes}`
      : onlyValidText`${selector.tag}${selector.id ? `#${selector.id}` : null}${classes}${attributesArr.join("")}`
  }
}
