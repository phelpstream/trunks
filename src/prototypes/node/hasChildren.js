export default function($) {
  return function hasChildren() {
    return this.children().length > 0 ? this.children() : false
  }
}
