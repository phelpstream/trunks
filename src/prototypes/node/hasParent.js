export default function($) {
  return function hasParent() {
    return this.parent().length > 0 ? this.parent() : false
  }
}
