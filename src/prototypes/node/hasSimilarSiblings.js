export default function($) {
  return function hasSimilarSiblings() {
    return this.parent().find(this)
  }
}
