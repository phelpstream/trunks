export default function($){
	return function texts(){
		return this.contents().filter((index, elem) => {
			if (elem.type === "text" && elem.data.replace(/\s+/gim, "").length > 0) return true
			return false
		})
	}
}