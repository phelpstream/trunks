import { isObject, isString, isArray } from "lodash"

export default function($) {
  return function json(options = {}) {
    let stringify = options.stringify || false
    let tagNameFilters = options.tagNameFilters || ["style", "script"]
    let typeFilters = options.typeFilters || []

    let outputResult = {}
    let startingNode = $(this)

    let iterateOnTheNode = function(node, output) {
      if (node) {
        $(node)
          .childrenWithTexts()
          .each(function(index, element) {
            let hasChildren = $(this).hasChildren()
            let { tagName, name, type, attribs } = $(this).get(0)
            let key = `${name || type}_${index}`
            let text = $(this).text()

            if (tagNameFilters.includes(tagName)) return
            if (typeFilters.includes(type)) return

            if (!hasChildren) {
              output[key] = text || attribs || {}
            } else {
              output[key] = {}
              iterateOnTheNode(this, output[key])
            }
          })
      } else {
        return null
      }
    }

    iterateOnTheNode(startingNode, outputResult)

    if (stringify) {
      return JSON.stringify(outputResult, null, 2)
    }

    return outputResult
  }
}
