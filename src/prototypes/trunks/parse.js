import { isObject, forOwn, isString, isArray } from "lodash"

import FramePropertyParserFactory from "./parse/dom/parsers/frameproperty.parser"
import StringParserFactory from "./parse/dom/parsers/string.parser"
import ArrayParserFactory from "./parse/dom/parsers/array.parser"
import ObjectParserFactory from "./parse/dom/parsers/object.parser"

export default function($) {
  return function parse(frame, options = {}) {
    let stringify = options.stringify || false

    let $trunks = this.trunks()
    const FramePropertyParser = FramePropertyParserFactory($trunks)
    const StringParser = StringParserFactory($trunks)
    const ArrayParser = ArrayParserFactory($trunks)
    const ObjectParser = ObjectParserFactory($trunks)

    let outputResult = {}
    let startingNode = $(this)

    let setActionsOnIteration = function(obj, output, node) {
      if (isObject(obj)) {
        forOwn(obj, function(value, property) {
          let FrameProperty = FramePropertyParser.isFrameProperty(property)

          if (FrameProperty.isTrue) {
            FramePropertyParser.getData($)(value, FrameProperty.propertyName, $(node), output, setActionsOnIteration)
          } else {
            if (value && isString(value)) {
              // Extract the data from the selector
              let tempResult = StringParser.getData($)(value, $(node))
              output[property] = tempResult
              // if (tempResult) output[property] = tempResult
            } else if (value && isArray(value)) {
              if (value.length === 1) {
                let tempResult = ArrayParser.String.getData($)(value, $(node))
                output[property] = tempResult
              } else {
                // Concatenate
                let tempResults = []
                for (let unitValue of value) {
                  let unitTempResult = StringParser.getData($)(unitValue, $(node))
                  if (unitTempResult) tempResults.push(unitTempResult)
                }
                output[property] = tempResults.join(' ')
              }
              // if (tempResult) output[property] = tempResult
            } else if (value && isObject(value)) {
              let tempResult = null
              tempResult = ObjectParser.getData($)(value, property, $(node), output, setActionsOnIteration)
              // output[property] = tempResult
              // if (tempResult) output[property] = tempResult
            }
          }
        })
      } else {
        return null
      }
    }

    setActionsOnIteration(frame, outputResult, startingNode)

    if (stringify) {
      return JSON.stringify(outputResult, null, 2)
    }

    return outputResult
  }
}
