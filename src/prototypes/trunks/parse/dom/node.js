import { isArray, isEmpty, isNumber, isString, isObject } from "lodash"
import selector from "./selector"

export default function($trunks) {
  const { getSelectorActions } = selector($trunks)
  const { NODE_EDITABLEVALUE, FNACTIONS_PREFIX } = $trunks.consts

  let getData = function($) {
    return function(node, actions = []) {
      $(node)[NODE_EDITABLEVALUE] = $(node).text()

      if (isArray(actions)) {
        // Detect an image automatically if no attribute in actions queue
        let { tagName, name, nodeName, localName } = Object.assign({ tagName: null, name: null, nodeName: null, localName: null }, $(node).get(0))

        let isImageTag = [tagName, name, nodeName, localName].some(
          value =>
            isString(value) &&
            value
              .trim()
              .toLowerCase()
              .includes("img")
        )

        // If node is an image
        if (isImageTag) $(node)[NODE_EDITABLEVALUE] = $(node).attr("src")

        // Play actions
        actions.forEach(function(action) {
          let { name, params, multiple } = action
          if (!node || !isObject(node)) console.error("Humm, node isn't really in good shape", node)
          if ($(node)[`${FNACTIONS_PREFIX}${name}`]) {
            let options = { multiple }
            node = $(node)[`${FNACTIONS_PREFIX}${name}`](params, options)
          }
        })
      }

      return $(node)[NODE_EDITABLEVALUE]
    }
  }

  let getNodes = function($) {
    return function(selector, node, { multipleNodes = false, nodeIndex = -1, textNodes = false, selfNode = false } = {}) {
      let nodes = null

      if (selector.trim().length > 0) {
        nodes = $(node).find(selector)
      } else {
        nodes = node
      }

      if (textNodes) nodes = $(nodes).texts()

      if (!multipleNodes && nodeIndex !== -1) {
        if (isNumber(+nodeIndex)) {
          nodes = $(nodes).get(+nodeIndex - 1)
        } else if (isString(nodeIndex)) {
          if (nodeIndex.includes("last")) {
            nodes = $(nodes).last()
          }
        }
      } else if (!multipleNodes) {
        nodes = $(nodes).first()
      }

      return nodes
    }
  }

  let getDataForEachNode = function($) {
    return function(selector, node, { multipleNodes = false } = {}) {
      let { actions, cleanSelector, nodeIndex, textNodes, selfNode } = getSelectorActions(selector)
      let data = []

      let nodes = getNodes($)(cleanSelector, node, { multipleNodes, nodeIndex, textNodes })

      if (nodes) {
        $(nodes).each(function(i, n) {
          let partialData = getData($)($(n), actions)
          if (partialData) data.push(partialData)
        })
        if (isArray(data) && data.length === 1) data = data[0]
        if (data.length === 0) data = null
      } else {
        data = null
      }

      return data
    }
  }

  return {
    getNodes,
    getDataForEachNode
  }
}
