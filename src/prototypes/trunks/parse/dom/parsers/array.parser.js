import { isArray } from "lodash"
import node from "./../node"

export default function($trunks) {
  const { getDataForEachNode } = node($trunks)

  let updateSelectorIfArray = function(selector) {
    if (isArray(selector)) {
      return {
        ok: true,
        firstSelector: selector[0],
        multipleNodes: true
      }
    }

    return {
      ok: false
    }
  }

  let String = {
    getData: function($) {
      return function(selector, node) {
        let { firstSelector, multipleNodes } = updateSelectorIfArray(selector)
        return getDataForEachNode($)(firstSelector, node, { multipleNodes })
      }
    }
  }

  return { String }
}
