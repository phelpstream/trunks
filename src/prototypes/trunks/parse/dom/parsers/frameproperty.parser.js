import { isArray, isString } from "lodash"
import node from "./../node"

import frameProperties from "./../../frame.properties"

export default function($trunks) {
  const { getNodes } = node($trunks)

  let isProperty = function(property, propertiesList = frameProperties) {
    let pList = propertiesList, // local declaration just in case
      status = false,
      propertyName = null,
      partOfText = false,
      referenceName = null

    Object.keys(pList).forEach(function(p) {
      pList[p].forEach(function(item) {
        if (item.includes("*")) {
          item = item.replace("*", "")
          partOfText = true
        }
        if ((partOfText && property.includes(item)) || item === property) {
          status = true
          propertyName = p
          referenceName = property.replace(item, "")
          return true
        }
      })
    })
    return {
      isTrue: status,
      propertyName: propertyName,
      referenceName: referenceName
    }
  }

  let isFrameProperty = property => isProperty(property)

  let getPropertyValue = function(obj, propertyName, propertiesList) {
    let pList = propertiesList
    let propertyNames = null
    let result = null
    let including = false

    if (pList[propertyName]) {
      propertyNames = pList[propertyName]

      Object.keys(obj).forEach(function(property) {
        propertyNames.forEach(function(name) {
          if (name.includes("*")) {
            name = name.replace("*", "")
            including = true
          } else {
            including = false
          }
          if ((including && property && property.startsWith(name)) || name === property) {
            result = obj[property]
            return true
          }
        })
      })

      return result
    } else {
      return false
    }
  }

  let getFramePropertyValue = function(obj, propertyName) {
    return getPropertyValue(obj, propertyName, frameProperties)
  }

  let getData = function($) {
    return function(value, propertyName, node, output, callback) {
      if (propertyName === "group") {
        let selector = getFramePropertyValue(value, "selector")
        let data = getFramePropertyValue(value, "data")
        let multiple = false
        if (isArray(data)) {
          multiple = true
        }

        if (selector && data) {
          if (isString(selector)) {
            let nextNode = getNodes($)(selector, node, {
              multiple
            })
            callback(data, output, nextNode)
          }
        }
      }
    }
  }

  return {
    getData,
    getFramePropertyValue,
    getPropertyValue,
    isFrameProperty,
    isProperty
  }
}
