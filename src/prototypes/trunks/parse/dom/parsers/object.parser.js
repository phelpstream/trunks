/**
 * All the functions required to parse Object containsData
 */

import { isArray, isString, isObject } from "lodash"

import node from "./../node"
import FramePropertyParserFactory from "./frameproperty.parser"
import StringParserFactory from "./string.parser"
import ArrayParserFactory from "./array.parser"

export default function($trunks) {
  const FramePropertyParser = FramePropertyParserFactory($trunks)
  const StringParser = StringParserFactory($trunks)
  const ArrayParser = ArrayParserFactory($trunks)
  const { getNodes } = node($trunks)

  let getData = function($) {
    return function(value, property, node, output, callback) {
      let containsSelector = FramePropertyParser.getFramePropertyValue(value, "selector")
      let containsData = FramePropertyParser.getFramePropertyValue(value, "data")
      let containsActions = FramePropertyParser.getFramePropertyValue(value, "actions")

      let multipleNodes = false
      if (isArray(containsData) && !(isArray(containsSelector) && containsSelector.length > 1)) {
        multipleNodes = true
        containsData = containsData[0]
      }
      // Contains Selector as an Array
      if (containsSelector && isArray(containsSelector)) {
        if (containsSelector.length === 1) {
          let tempResult = ArrayParser.String.getData($)(containsSelector, $(node))
          output[property] = tempResult
        } else {
          // Concatenate
          let tempResults = []
          for (let unitValue of containsSelector) {
            let unitTempResult = StringParser.getData($)(unitValue, $(node))
            if (unitTempResult) tempResults.push(unitTempResult)
          }
          output[property] = tempResults.join(" ")
        }
        // Contains Selector && Contains Data
      } else if (containsSelector && isString(containsSelector) && containsData) {
        if (isString(containsData)) {
          let nextNode = getNodes($)(containsSelector, $(node), {
            multipleNodes
          })
          return StringParser.getData($)(containsData, $(nextNode))
        } else if (isObject(containsData)) {
          let nextNode = getNodes($)(containsSelector, node, {
            multipleNodes
          })

          if (multipleNodes) {
            output[property] = []
            $(nextNode).each(function(index, n) {
              output[property][index] = {}
              callback(containsData, output[property][index], $(n))
            })
          } else {
            output[property] = {}
            callback(containsData, output[property], nextNode)
          }
        }
        // Contains Selector as a String
      } else if (containsSelector && isString(containsSelector)) {
        return StringParser.getData($)(containsSelector, $(node))
        if (isString(containsSelector)) {
        } else {
        }

        // logger.debug(sep)
        // Pass on the object for organization
      } else if (!containsSelector) {
        output[property] = {}
        callback(value, output[property], node)
      }
    }
  }
  return { getData }
}
