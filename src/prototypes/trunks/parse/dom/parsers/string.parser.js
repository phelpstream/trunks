/**
 * All the functions required to parse String data
 */

import node from "./../node"

export default function($trunks) {
  const { getDataForEachNode } = node($trunks)
  let getData = $ => (selector, node) => getDataForEachNode($)(selector, node)
  return { getData }
}
