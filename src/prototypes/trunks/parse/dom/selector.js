import { has, isArray, isString } from "lodash"
// import { selfnode } from "./../selector.properties"

export default function($trunks) {
  const { getRegexGroupValues, unescape } = $trunks.helpers.regex
  const { splitOnLastSeenChar, sideSpacedWord } = $trunks.helpers.string
  const { MULTIPLE_SYMBOL, ACTIONS_DELIMITER } = $trunks.consts
  const { MULTI_BRACKETS, FUNCTIONS, PARAMETERS_IN_PARENTHESIS, WIDER_PARENTHESIS } = $trunks.consts.regexes

  let getNodeIndexFromSelector = function(selector) {
    let indexMatches = selector.trim().match(/\s\d+$/gm)
    if (indexMatches && indexMatches[0])
      return {
        nodeIndex: +indexMatches[0] || 1,
        cleanSelector: selector
          .trim()
          .slice(0, -indexMatches[0].length)
          .trim()
      }
    return null
  }

  let getTextNodesFromSelector = function(selector) {
    if (selector && selector.trim().endsWith("$text"))
      return {
        textNodes: true,
        cleanSelector: selector.replace("$text", "")
      }
    return null
  }

  let getSelectorActions = function(selector) {
    let result = {
      cleanSelector: selector,
      // selfNode: false,
      actions: [],
      nodeIndex: 1,
      textNodes: false
    }

    // console.log("selector", selector)

    if (selector && selector.includes(ACTIONS_DELIMITER)) {
      result.cleanSelector = selector.split(ACTIONS_DELIMITER)[0].trim()
      let actionsString = selector.split(ACTIONS_DELIMITER)[1].trim()
      let actionsArr = actionsString.match(FUNCTIONS)

      for (let action of actionsArr) {
        let betweenParanthesis = getRegexGroupValues(action, WIDER_PARENTHESIS)[0] || null
        let actionParams = betweenParanthesis ? getRegexGroupValues(betweenParanthesis, PARAMETERS_IN_PARENTHESIS) : null
        if (actionParams)
          actionParams = actionParams.map(actionParam => {
            if (actionParam.startsWith("'")) actionParam = actionParam.slice(1)
            if (actionParam.endsWith("'")) actionParam = actionParam.slice(0, -1)
            return actionParam
          })

        let actionToPush = {
          name: actionParams ? action.split("(")[0] : action,
          params: actionParams,
          multiple: action.endsWith(MULTIPLE_SYMBOL)
        }
        if (actionToPush.multiple) actionToPush.name.slice(0, -1)
        result.actions.push(actionToPush)
      }
      // result.actions.reverse()
    }

    let checkNodeIndexInSelector = getNodeIndexFromSelector(result.cleanSelector)
    if (checkNodeIndexInSelector) {
      result.cleanSelector = checkNodeIndexInSelector.cleanSelector
      result.nodeIndex = checkNodeIndexInSelector.nodeIndex
    }

    let checkTextNodesInSelector = getTextNodesFromSelector(result.cleanSelector)
    if (checkTextNodesInSelector) {
      result.cleanSelector = checkTextNodesInSelector.cleanSelector
      result.textNodes = checkTextNodesInSelector.textNodes
    }

    // if (selfnode.some(selfnodeDeclaration => result.cleanSelector.trim().startsWith(selfnodeDeclaration))) {
    //   result.selfNode = true
    //   selfnode.forEach(function(selfnodeDeclaration) {
    //     if (result.cleanSelector.includes(selfnodeDeclaration)) result.cleanSelector = result.cleanSelector.replace(selfnodeDeclaration, "")
    //   })
    // }

    return result
  }

  return { getSelectorActions }
}
