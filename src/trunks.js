import { isObject, isString } from "lodash"
import cheerio from "cheerio"

import addPrototypes from "./functions/addPrototypes"

// cheerios prototypes helpers
import nodeTexts from "./prototypes/node/texts"
import nodeChildrenWithTexts from "./prototypes/node/childrenWithTexts"
import nodeHasChildren from "./prototypes/node/hasChildren"
import nodeHasParent from "./prototypes/node/hasParent"
import nodeHasSimilarSiblings from "./prototypes/node/hasSimilarSiblings"
import nodeGetSelector from "./prototypes/node/getSelector"

// default prototypes
import protoParse from "./prototypes/trunks/parse"
import protoScrape from "./prototypes/trunks/scrape"
import protoJson from "./prototypes/trunks/json"
import protoSelfTrunks from "./prototypes/trunks/trunks"

// default actions
import Actions from "./actions/actions"
import defaultActions from "./actions/default"

// consts
import regexes from "./config/regex"
import consts from "./config/consts"

// helpers
import * as regexHelpers from "./helpers/regex"
import * as stringHelpers from "./helpers/string"
import * as iterateHelpers from "./helpers/iterate"

/**
 * Trunks - custom implementation of cheerio
 * @constructor
 * @param {(string|Object)} input - Can be HTML or the $ cheerio itself
 * @param {Object} options - Trunks' options
 * @param {Object} cheerioOptions - Cheerio's options
 */

function trunks(input = "", trunksOptions = {}, cheerioOptions = {}) {
  if (input === "") throw Error("No input detected!")

  let $ = {}
  if (isObject(input)) $ = input
  else if (isString(input)) $ = cheerio.load(input, cheerioOptions)

  $.$trunks = {
    options: isObject(trunksOptions) ? trunksOptions : {},
    load: function({ node = null, actions = null } = {}) {
      if (node) addPrototypes($)(node)
      if (actions) Actions.load($)(actions)
    },
    consts: Object.assign(consts, { regexes }),
    helpers: {
      regex: regexHelpers,
      string: stringHelpers,
      iterate: iterateHelpers
    }
  }

  // Load cheerio extension
  $.$trunks.load({
    node: {
      texts: nodeTexts,
      childrenWithTexts: nodeChildrenWithTexts,
      hasChildren: nodeHasChildren,
      hasParent: nodeHasParent,
      hasSimilarSiblings: nodeHasSimilarSiblings,
      getSelector: nodeGetSelector
    },
    actions: [defaultActions]
  })

  // Load trunks prototypes
  $.$trunks.load({
    node: {
      trunks: protoSelfTrunks,
      parse: protoParse,
      scrape: protoScrape,
      json: protoJson
    }
  })

  return $
}

export default trunks
