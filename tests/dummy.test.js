const expect = require("expect")
const trunks = require("./../lib/trunks")
const fs = require("fs")
const path = require("path")
const url = require("url")

let pagePath = "./ressources/dummy.html"
let rootUrl = "http://dummy-page.com"

let html = fs.readFileSync(path.resolve(__dirname, pagePath), { encoding: "UTF-8" })

let $ = trunks(html, { rootUrl }, {})

describe("[Page] Dummy", () => {
  describe("[selector]", () => {
    it("(text) should get the inner text", () => {
      let frame = {
        title: "h2"
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        title: "Pricing"
      })
    })
  })

  describe("[attribute]", () => {
    it("(specific) should get the price attribute", () => {
      let frame = {
        proPrice: ".planName:contains('Pro') + span @ attr(price)"
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        proPrice: "39.00"
      })
    })

    it("(link) should get the link url", () => {
      let frame = {
        link: ".mainLink @ attr(href)"
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        link: "some/url/to/somewhere"
      })
    })

    it("(img) should automatically get the src attribute", () => {
      let frame = {
        picture: ".picture"
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        picture: "somepath/to/image.png"
      })
    })
  })

  describe("[extractor]", () => {
    it("(absoluteUrl) should get the absolute link url", () => {
      let frame = {
        link: ".mainLink @ attr(href) absoluteUrl"
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        link: url.resolve(rootUrl, "some/url/to/somewhere")
      })
    })

    it("(number) should get the number", () => {
      let frame = {
        number: "#pricing .item:nth-child(2) .planPrice @ number"
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        number: "39"
      })
    })

    it("(website) should extract a website from the text", () => {
      let frame = {
        website: "#more @ website"
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        website: "www.google.com"
      })
    })

    it("(phone) should get the american phone number", () => {
      let frame = {
        telephone: "[itemprop=usaphone] @ telephone"
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        telephone: "(912) 148-456"
      })
    })

    it("(phone) should get the french phone number", () => {
      let frame = {
        telephone: "[itemprop=frphone] @ telephone"
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        telephone: "+332 38 30 37 90"
      })
    })

    it("(email) should get the email", () => {
      let frame = {
        email: "[itemprop=email] @ email"
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        email: "lspurcell@suddenlink.net"
      })
    })

    it("(html) should get the html", () => {
      let frame = {
        inner: ".popup @ html"
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        inner: "<span>Some inner content</span>"
      })
    })
  })

  describe("[parse]", () => {
    it("(date regex) should get the parsed date dd/mm/yyyy from regex", () => {
      let frame = {
        data: ".date @ match('\\d{1,2}\/\\d{1,2}\/\\d{2,4}', img)"
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        data: "04/02/2017"
      })
    })
  })

  describe("[nested object]", () => {
    it("(_selector & _data) should get json object with parent > child", () => {
      let frame = {
        pricing: {
          $s: "#pricing .item",
          $d: {
            name: ".planName",
            price: ".planPrice"
          }
        }
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        pricing: {
          name: "Hacker",
          price: "Free"
        }
      })
    })
  })

  describe("[array]", () => {
    it("(nested) should get json object with parent > childs []", () => {
      let frame = {
        pricing: {
          _s: "#pricing .item",
          _d: [
            {
              name: ".planName",
              price: ".planPrice"
            }
          ]
        }
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        pricing: [
          {
            name: "Hacker",
            price: "Free"
          },
          {
            name: "Pro",
            price: "$39"
          }
        ]
      })
    })
  })

  describe("[grouped]", () => {
    it("(object) should get the data within the first li item", () => {
      let frame = {
        _g: {
          _s: "#pricing .item",
          _d: {
            name: ".planName",
            price: ".planPrice @ attr(price)",
            image: {
              url: "img",
              link: "a @ attr(href)"
            }
          }
        }
      }

      let output = $("body").scrape(frame)

      expect(output).toEqual({
        name: "Hacker",
        price: "0",
        image: {
          url: "./img/hacker.png",
          link: "/hacker"
        }
      })
    })
  })

  // describe("Full examples", () => {
  //   it("should get the pricing list + details", () => {
  //     let frame = {
  //       pricing: {
  //         _s: "#pricing .item",
  //         _d: [
  //           {
  //             name: ".planName",
  //             price: ".planPrice @ price",
  //             image: {
  //               url: "img",
  //               link: "a @ href"
  //             }
  //           }
  //         ]
  //       }
  //     }

  //     let output = $("body").scrape(frame)

  //     expect(output).toEqual({
  //       pricing: [
  //         {
  //           name: "Hacker",
  //           price: "0",
  //           image: {
  //             url: "./img/hacker.png",
  //             link: "/hacker"
  //           }
  //         },
  //         {
  //           name: "Pro",
  //           price: "39.00",
  //           image: {
  //             url: "./img/pro.png",
  //             link: "/pro"
  //           }
  //         }
  //       ]
  //     })
  //   })
  // })
})
